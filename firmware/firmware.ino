// mutantC keyboard v2.3

#include <Keypad.h>
#include <HID-Project.h>

#define LED_DISPLAY 10
#define POWER_MAIN 16

const byte ROWS = 5;
const byte COLS = 11;
static bool keymapState = 0;

#define FIRMWARE_VERSION 2300

#define KEYS_TILDE      KEY_TILDE|MOD_LEFT_SHIFT       /* ~ */
#define KEYS_EXCL       KEY_1|MOD_LEFT_SHIFT           /* ! */
#define KEYS_AT         KEY_2|MOD_LEFT_SHIFT           /* @ */
#define KEYS_HASH       KEY_3|MOD_LEFT_SHIFT           /* # */
#define KEYS_DOLLAR     KEY_4|MOD_LEFT_SHIFT           /* $ */
#define KEYS_PERCENT    KEY_5|MOD_LEFT_SHIFT           /* % */
#define KEYS_CARET      KEY_6|MOD_LEFT_SHIFT           /* ^ */
#define KEYS_AMP        KEY_7|MOD_LEFT_SHIFT           /* & */
#define KEYS_ASTERISK   KEY_8|MOD_LEFT_SHIFT           /* * */
#define KEYS_LRBRACKET  KEY_9|MOD_LEFT_SHIFT           /* ( */
#define KEYS_RRBRACKET  KEY_0|MOD_LEFT_SHIFT           /* ) */
#define KEYS_UNDERSCORE KEY_MINUS|MOD_LEFT_SHIFT       /* _ */
#define KEYS_PLUS       KEY_EQUAL|MOD_LEFT_SHIFT       /* + */
#define KEYS_LCBRACKET  KEY_LEFT_BRACE|MOD_LEFT_SHIFT  /* { */
#define KEYS_RCBRACKET  KEY_RIGHT_BRACE|MOD_LEFT_SHIFT /* } */
#define KEYS_VBAR       KEY_BACKSLASH|MOD_LEFT_SHIFT   /* \ */
#define KEYS_COLON      KEY_SEMICOLON|MOD_LEFT_SHIFT   /* : */
#define KEYS_DQUOTE     KEY_QUOTE|MOD_LEFT_SHIFT       /* " */
#define KEYS_LABRACKET  KEY_COMMA|MOD_LEFT_SHIFT       /* < */
#define KEYS_RABRACKET  KEY_PERIOD|MOD_LEFT_SHIFT      /* > */
#define KEYS_QMARK      KEY_SLASH|MOD_LEFT_SHIFT       /* ? */

#define POWEROFF        0xffff
#define KMAP_SWITCH     0xfffe
#define NC              0

static uint16_t keymapSymbols[] = {
             NC,     POWEROFF,            'E',             'E',            'E',             'E',            'E',            'E',            'E',    KMAP_SWITCH, NC,
        KEY_ESC,    KEYS_EXCL,        KEYS_AT,       KEYS_HASH,    KEYS_DOLLAR,    KEYS_PERCENT,     KEYS_CARET,       KEYS_AMP, KEYS_LRBRACKET, KEYS_RRBRACKET, KEY_LEFT,
        KEY_TAB,    KEYS_VBAR,         KEY_UP, KEYS_UNDERSCORE,     KEYS_TILDE,      KEYS_COLON,  KEY_SEMICOLON,    KEYS_DQUOTE,      KEY_QUOTE,      KEY_TILDE, KEY_RIGHT,
  KEY_CAPS_LOCK,     KEY_LEFT,       KEY_DOWN,       KEY_RIGHT, KEY_LEFT_BRACE, KEY_RIGHT_BRACE, KEYS_LCBRACKET, KEYS_RCBRACKET, KEYS_LABRACKET, KEYS_RABRACKET, KEY_BACKSPACE,
  KEY_LEFT_CTRL, KEY_LEFT_GUI,      KEYS_PLUS,       KEY_MINUS,      KEY_SLASH,   KEYS_ASTERISK,      KEY_EQUAL,      KEY_COMMA,     KEY_PERIOD,     KEYS_QMARK, KEY_ENTER
};

static uint16_t keymapAlpha[] = {
             NC,     POWEROFF, KEY_PRINTSCREEN,   KEY_INSERT,   KEY_DELETE,   KEY_HOME,   KEY_END,   KEY_PAGE_UP, KEY_PAGE_DOWN, KMAP_SWITCH, NC,
          KEY_1,        KEY_2,           KEY_3,        KEY_4,        KEY_5,      KEY_6,     KEY_7,         KEY_8,         KEY_9,       KEY_0, KEY_LEFT,
          KEY_Q,        KEY_W,           KEY_E,        KEY_R,        KEY_T,      KEY_Y,     KEY_U,         KEY_I,         KEY_O,       KEY_P, KEY_RIGHT,
  KEY_CAPS_LOCK,        KEY_A,           KEY_S,        KEY_D,        KEY_F,      KEY_G,     KEY_H,         KEY_J,         KEY_K,       KEY_L, KEY_BACKSPACE,
  KEY_LEFT_CTRL, KEY_LEFT_GUI,           KEY_Z,        KEY_X,        KEY_C,      KEY_V,     KEY_B,         KEY_N,         KEY_M,   KEY_SPACE, KEY_ENTER
};

static char dummyKeypad[ROWS][COLS] = {
  { 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10},
  {11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21},
  {22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32},
  {33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43},
  {44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54}
};

byte rowPins[ROWS] = {A2, 15, 0, 1, A3};
byte colPins[COLS] = {14, A0, 9, 8, 7, 6, 5, 4, 3, 2, A1};

Keypad Dummy = Keypad(makeKeymap(dummyKeypad), rowPins, colPins, ROWS, COLS);

/* Temperature sensor offset */
#define TEMP_OFFSET -7
/* Temperature in Kelvins when to power off */
static uint16_t temp_thresh = 323;
/* Last temperature measured */
static uint16_t last_temp;

static void read_temp(void)
{
  /* Set reference to 2.56V and MUX to temp sensor */
  ADMUX = (1<<REFS1) | (1<<REFS0) | (1<<MUX2) | (1<<MUX1) | (1<<MUX0);
  ADCSRB = (1<<MUX5);
  /* Set ADC prescaler and enable ADC */
  ADCSRA = (1<<ADPS2) | (1<<ADPS1) | (1<<ADEN);
  /* The CPU is slow so we do not have to wait here */
  /* Start an ADC conversion */
  ADCSRA |= (1<<ADSC);
  /* Wait for ADC conversion to finish */
  while ((ADCSRA & (1<<ADSC)));

  last_temp = ADCW + TEMP_OFFSET;

  if (last_temp > temp_thresh)
    poweroff();
}

void setup(void)
{
  /*  Turn on the FET that powers on the board and RPI first */
  pinMode(POWER_MAIN, OUTPUT);
  digitalWrite(POWER_MAIN, HIGH);

  pinMode(LED_DISPLAY, OUTPUT);

  /*
   * Slow down CPU so that we do not consume too much power
   *
   * With this the power goes down to about 8mA for the CPU.
   */
  noInterrupts();
  CLKPR = 0x80;  // enable change of the clock prescaler
  CLKPR = 0x04;  // divide frequency by 16
  interrupts();

  /* Compensate for the slow CPU */
  Dummy.setHoldTime(1);
  Dummy.setDebounceTime(0);

  Dummy.addEventListener(keypadEvent);

  read_temp();

  Serial.begin(9600);
}

static void poweroff(void)
{
  uint8_t i;

  /* Ask RPi to powerdown */
  Keyboard.write(CONSUMER_POWER);

  /* Wait for about 16 seconds */
  for (i = 0; i < 20; i++) {
    digitalWrite(LED_DISPLAY, HIGH);
    delay(25);
    digitalWrite(LED_DISPLAY, LOW);
    delay(25);
  }

  /* And finally turn off the power */
  digitalWrite(POWER_MAIN, LOW);
}

static void selectAlphabet(void)
{
  digitalWrite(LED_DISPLAY, LOW);
  keymapState = 0;
}

static void selectSymbols(void)
{
  digitalWrite(LED_DISPLAY, HIGH);
  keymapState = 1;
}

static void switchKeymap(void)
{
  if (keymapState)
    selectAlphabet();
  else
    selectSymbols();
}

static uint16_t mapKey(char key)
{
  if (keymapState)
    return keymapSymbols[key];

  return keymapAlpha[key];
}

static void keypadEvent(KeypadEvent key)
{
  if (Dummy.getState() != PRESSED)
    return;

  uint16_t kcode = mapKey(key);

  switch (kcode) {
  case POWEROFF:
    poweroff();
  break;
  case KMAP_SWITCH:
    switchKeymap();
  break;
  }
}

static int left_shift_ref_cnt;

static void processKeys(void)
{
  if (!Dummy.getKeys())
    return;

  int i;

  for (i = 0; i < LIST_MAX; i++) {
    if (!Dummy.key[i].stateChanged)
      continue;

    uint16_t key = mapKey(Dummy.key[i].kchar);

    if (key == KMAP_SWITCH || key == POWEROFF)
      continue;

    switch (Dummy.key[i].kstate) {
    case PRESSED:
      if (key & MOD_LEFT_SHIFT) {
        if (!(left_shift_ref_cnt++))
          Keyboard.press(KEY_LEFT_SHIFT);
      }
      Keyboard.press((KeyboardKeycode)key);
    break;
    case RELEASED:
      if (key & MOD_LEFT_SHIFT) {
        if (!(--left_shift_ref_cnt))
          Keyboard.release(KEY_LEFT_SHIFT);
      }
      Keyboard.release((KeyboardKeycode)key);
    break;
    }
  }
}

static void write_u16_res(char type, uint16_t val)
{
  Serial.write(type);
  Serial.print(val);
  Serial.write('\n');
}

/*
 * Simple UART protocol for setting and getting data.
 * The message consists of a few bytes followed by a newline.
 *
 * To setup the serial port on RPi use:
 *
 * stty -F /dev/ttyACM0 9600 raw -clocal -echo icrnl
 *
 * Then you can read results and write commands with:
 *
 * cat /dev/ttyACM0
 * cat > /dev/ttyACM0
 *
 * cmd[0]:
 * ? - get value
 * ! - set value
 *
 * cmd[1]
 * T - internal temperature in Kelvin
 * P - poweroff threshold temperature in Kelvin
 * V - mutant hardware revision
 * F - firmware version
 *
 * cmd[2] - cmd[4]
 *  - optional command parameter
 *
 * '\n' - message end
 */
static void process_serial(void)
{
  static uint8_t cmd[6];
  static uint8_t lb;
  int b;

  while ((b = Serial.read()) > 0) {
    cmd[lb] = b;

    if (cmd[lb] == '\n')
      break;

    lb = (lb + 1) % 6;
  }

  if (cmd[lb] != '\n')
    return;

  switch (cmd[0]) {
  case '?':
    switch (cmd[1]) {
    case 'T':
      write_u16_res('T', last_temp);
    break;
    case 'P':
      write_u16_res('P', temp_thresh);
    break;
    case 'V':
      write_u16_res('V', 2);
    break;
    case 'F':
      write_u16_res('F', FIRMWARE_VERSION);
    break;
    case 'L':
      write_u16_res('L', digitalRead(LED_DISPLAY));
    break;
    }
  break;
  case '!':
    switch (cmd[1]) {
      case 'P':
        cmd[lb] = 0;
        temp_thresh = atoi(cmd+2);
      break;
      case 'L':
        if (atoi(cmd+2))
          digitalWrite(LED_DISPLAY, HIGH);
        else
          digitalWrite(LED_DISPLAY, LOW);
      break;
    }
  break;
  }

  lb = 0;
}

void loop(void)
{
  static uint16_t i = 0;

  processKeys();

  process_serial();

  /* Read temperature every few seconds */
  if (i++ == 30000) {
    read_temp();
    i = 0;
  }
}
